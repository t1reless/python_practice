#obligatory first function 
#lecture one basic functions
def hello_world():
    print("hello world")

#basic application of the input function as per CS50 one
#each one is an incrementally more elegant expression
def string_practice1():
    name = input("What's your name? ") 
#using concatenation
    print("Hey there, " + name)
#using comma to pass an addtional argument, space removed stop redundancy 
    print("Hey there,", name)
#my personal favorite, the formatted string
    print(f"Hey there, {name}")
#throwing a monkey wrench into the default print parameters to create
#a monkey's idea of a string implementation
    print("Hey there, ", end="")
    print(name)
#using escape characters (misery)
    print("hello, \"friend\"")

#sanitizing string inputs
def string_practice2():
    name = input("What is your name? ")
#guarding against those with an abusive relationship with their 
#spacebar
    name = name.strip()
#keeping things capitalized, as they should be. 
    name = name.capitalize()
#But wait! What if they feed us their entire name?? 
    name = name.title()
#if I wanted to I could chain functions but ¯\_(ツ)_/¯
#how about greeting by only first name? 
    fname = name.split()

    print(f"Hey there, {name}")
#split function returns a list so the 0 is indexing properly
    print(f"Hey there, {fname[0]}")

#moving on to arithmetic! (integers and floats)
def int_add():
#i could implement the int typing on the variables but then i would
#fall prey to type errors
    x = int(input("What's x? "))
    y = int(input("What's y? "))
    z = x + y 
#mindblowing feature set, good god
#instead of z I could just add x and y under the print
    print(z)

#basic function to return the square of keyword
def square(x):
    y = int(x)
    return y * y

def float_add():
#in the unlikely case some weirdo wants to add non integers (gross)
    x = float(input("What's x? "))
    y = float(input("What's y? "))
    z = x + y 
#printing out the raw number
    print(z)
#printing the number as a rounded number
    print(round(x)+round(y))
#printing the number with the customary commas to easily read large
#ones 
    print(f"{z:,}")

def float_div():
    x = float(input("What's x? "))
    y = float(input("What's y? "))
    z = (x / y)

    print(z)
#for those of us who like daintier numbers...
    print(f"{z:.2f}")

#writing our own function (i've neeeeever done this before)
#interesting note, you can define a default value for any parameter
#it's useful to place the entire block of code within the "main" function
#so that all function names are resolved at runtime and calling them becomes placement agnostic
def hey(name="you"):
    print(f"Hey there, {name.title().strip()}")


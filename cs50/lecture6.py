def names_temp():
    names = [] 
    for _ in range(3):
        name = input("What's your name? ")
        names.append(name)
    
    for name in sorted(names):
        print(f"Hello, {name}")

def names_to_text_file():
    names = []
    for _ in range(3):
        name = input("What's your name? ")
        names.append(name)

    file = open("names_dump.txt", "w")
    for n in sorted(names):
        file.write(f"{n} \n")

def better_text_to_file():
    names = []
    for _ in range(3):
        name = input("What is your name? ")
        names.append(name)
#using the with keyword automatically closes the file 
#while this isn't always necessary, it is best practice.
    with open("names_dump.txt") as file: 
        file.write(f"{name}\n")

def text_from_file():
    with open("names_dump.txt","r") as file: 
#this opens the file and reads all of the lines serially
#then the 'lines' variable stores the contents of the file
#as a list with each new line being a new entry
#in this way a text file is almost analogous to a list..?
        lines = file.readlines()

        for line in lines:
            print(f"Hello, {line.rstrip()}")

def better_text_from_file():
    with open("names_dump.txt", "r") as file:
        for line in file: 
            print("Hello, ", line.rstrip)

#using this same general idea but with sorting

def better_text_read_sorted():
    names = []
    with open("names_dump.txt") as file: 
        for line in file: 
            names.append(line.rstrip())
    for name in sorted(names):
        print("Hello, ", name)

def text_reading_csv():
    with open("names.csv") as file: 
        for line in file: 
            #clumsy way of doing this
            row = line.rstrip().split(",")
            #sick way of doing this 
            fname, lname = line.rstrip().split(",")
            print(f"First Name: {fname} \n Last Name: {lname} ")

#used to retrieve the first name of a given dictionary
#stored in the people list
def get_name(person):
    return person["fname"]

def text_reading_csv_sorted():
#initialize an empty list to store dicts
    people = []
#opening our sample text file
    with open("names.csv") as file: 
        for line in file: 
#storing the two values fname and lname as variables 
            fname, lname = line.rstrip().split(",")
#creating a dictionary storing the values as key pairs 
            person = {"fname":fname, "lname":lname}
#appending the dictionary to the list
            people.append(person)
#the key value allows you to choose which element 
#you want to sort the entries by. below is baby's first lambda 
#   for p in sorted(people, key=lambda person: person["fname"])
    for p in sorted(people, key=get_name):
        print(f"{p['fname']}'s last name is {p['lname']}")

#apparently there's actually a csv library woah
def csv_module_example():
    import csv
    people = []
    with open("names.csv") as file: 
        reader = csv.reader(file)
        for fname, lname in reader:
            people.append({"fname":fname, "lname":lname})


def dict_reader():
    import csv
    people = []
    with open("names.csv") as file:
        reader = csv.DictReader(file)
        for row in reader:
            people.append({"fname":row["fname"], "lname":row["lname"]})
    for p in sorted(people, key = lambda name: name["fname"]):
        print(f"First: {p['fname']} and last: {p['lname']}")


def csv_writer():
    import csv
    fname = input("First name? ")
    lname = input("Last name? ")
    with open("names.csv", "a") as file: 
        writer = csv.writer(file)
        writer.writerow([fname,lname])

def csv_dict_writer():
    import csv
    fname = input("First name? ")
    lname = input("Last name? ")
    with open("names.csv", "a") as file:
        writer = csv.DictWriter(file, fieldnames=["fname","lname"])
        writer.writerow({"fname": fname, "lname":lname}) 


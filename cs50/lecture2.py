#loops
#in this particular example I have been asked to 
#make a cat meow n number of times 
def bad_kitty():
    print("meow")
    print("meow")
    print("meow")

#while keyword implemented to describe how long to 
#perform an action. this is done with boolean
#values
def better_kitty():
    i = 3
    while i != 0:
        print("meow") 
        i = i = 1

#for loop implementation alongside the list 
def bad_list_kitty():
    for i in [0,1,2]:
        print("meow")

def better_list_kitty():
    for i in range(3):
        print("meow")

#minor pythonic improvement is possible
#using an underscore signals that the variable is just an iterator
def pythonic_kitty():
   for _ in range(3):
       print("meow") 

#even more pythonic but god awful for readability
def just_a_python():
    print("meow \n"*3,end="")

#specific meow quantity
def obedient_cat():
    while True:
        n = int(input("What's n? "))
        if n > 0: 
            break
    for _ in range(n):
        print("meow")

#dealing with the list data type
def bad_students():
    students = ["Jack", "Nat", "Stefan"]
    print(students[0])
    print(students[1])
    print(students[2])

def better_students():
    students = ["Jack", "Nat","Stefan"]
    for i in students:
        print(i)

#specifying the number of elements in a list to iterate
def weird_students():
    students = ["Jack", "Nat","Stefan"]
    for i in range(len(students)):
        print(i, students[i])

#hash maps!! specifically dictionaries (key value matching pair)
#telling the dorm of the student based on the name
def organized_students():
    students = {
        "Jack":"Urania","Nat":"Sunflower",
        "Stefan":"Uppers"
        }
#print my dorm 
    print(students["Jack"])

def more_organized_students():
    students = {
        "Jack":"Urania", "Nat":"Sunflower",
        "Stefan":"Uppers"}
    for student in students:
#calling print function which then prints the student
#name instantiated in the above operation and then passes
#the name into the index of the student list to get the living
#quarters
        print(student, students[student])

#creating a more complex dict
#this is just a precursor to matrices
#this is a list of dictionaries which will be called 
#by the index of the list element followed by
#the dict
def most_organized_students():
    students = [
        {"name":"Jack","dwelling":"Urania",
         "sport":"weightlifting"}, 
        {"name":"Nat","dwelling":"Sunflower",
          "sport":"Running"},
        {"name":"Stefan","dwelling": None,
         "sport":"Weightlifting"}
    ]
    for i in students:
        print(f"{i['name']} {i['dwelling']}")        

#mario game? idk harvard is buggin
def brick_stacker(height):
    for _ in range(height):
        print("#")

def brick_horizontal(width):
    print("?"*width)

def square_shape_print(width, height):
    for _ in range(height):
        print("#"*width)
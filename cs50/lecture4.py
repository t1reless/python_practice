#introduction to libraries &
#introduction to modules
#this gets us into the realm of importing 
def coin_toss():
    import random
    coin = random.choice(["heads","tails"])
    print(coin)

#using from keywords allows you to better control 
#for namespace collissions and makes your code 
#more readable
def better_coin_toss():
    from random import choice
    coin = choice(["heads","tails"])

#trying the shuffle function 
#curiously this function shuffles the list and does 
#not instantiate a new list so you have to be careful
#if it has to be persistent
def cards():
    from random import shuffle
    cards = ["Jack","Queen","King"]
    shuffle(cards)
    for card in cards: 
        print(card)

#statistics library 
def average():
    from statistics import mean
    scores = [100,90,48,104,200]
    print(mean(scores))

#!!!command line arguments, HIGHLY important!!!
def io_sys():
    from sys import argv
    from sys import exit
    try:
        print("Hello, my name is: ", argv[1])
    except:
        IndexError
        print("Hello, please pass your name.")
        exit()

#how to deal with a 'slice' of argv
#a slice is a subset of a data struct 
def sys_slicer():
    from sys import argv
#this allows you to exclude the first argument of 
#sys argv which is always the program name
    for arg in argv[1:]:
        print("Hello, my name is", )

#packages intro 
#packages are third party libraries 
#PyPI (python package index)
#implementing cowsay
#pip is python's package manager and can be used to 
#interface with this stuff
#i, however, am not going to deal with this nonsense
#because i don't care to set up a hypervisor 
#to create an ascii cow
#!!! revisit best practices for all of this!!!
def band_finder():
    import requests
    import sys
    import json
    if len(sys.argv) != 2:
        sys.exit()

    #defining a variable called response which is the json file passed to us by itunes based on their API
    #documentation with the 1st argument following the program name defining the artist. 
    #obviously this doesn't work with multi word band names 
    response = requests.get("https://itunes.apple.com/search?entity=song&limit=2&term=" + sys.argv[1])
    
    #storing the value we received (a list of dictionaries) as a variable which points to a json file
    o = response.json()
    print(json.dumps(o, indent=2))
    #iterates 
    for result in o["results"]:
        print(result["trackName"])
band_finder()
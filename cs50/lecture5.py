#creating unit tests 
#the purpose of these tests is to check that our
#function performs the operation we would like it 
def main():
    squarer_test()

def square(n):
    return n + n 

def squarer():
    try:
        x = int(input("What's x? "))
        print(f"x squared is, {x}")
    except:
        ValueError
        print("Feed me an integer, please.")

def squarer_test():
    assert square(2) == 4
    assert square(3) == 9

#thanks to pytest we have the ability to save ourselves
#the trouble of try except to the nth degree
#we can just assert a ton of things in the test file 
#and then run it as pytest and pytest will handle it
#likewise I can use pytest raises module to check to make sure 
#my exception handling works correctly
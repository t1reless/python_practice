#lecture1 working with conditionals
#> >= < <= == != all different mathematical conditionals
#greater than, greater or equal, less than, less than or equal,
#equal, and not equal to. 

#lecturer used a boolean algebra flowchart...worth checking out.
#this is called "control flow"
def badcompare():
    x = int(input("What's x? "))
    y = int(input("What's y? "))

    if x < y:
        print("x is less than y. ")
    if x > y: 
        print("x is greater than y.")
    if x == y: 
        print("x = y")

#implementing the same program but with elif for better 
#optimization
def bettercompare():
    x = int(input("What's x? "))
    y = int(input("What's y? "))
    
    if x < y:
        print("x is less than y")
    elif x > y:
        print("x is greater than y")
    elif x == y:
        print("x = y")

#even better implementation using the else conditional
def bestcompare():
    x = int(input("What's x? "))
    y = int(input("What's y? "))

    if x < y: 
        print("x is less than y")
    elif x > y: 
        print("x is greater than y")
    else:
        print("x = y")

#working with the or conditional
def orcompare():
    x = int(input("What is x? "))
    y = int(input("What is y? "))
    if x < y or x > y:
        print("x does not equal y")
    else:
        print("x = y")

def betterorcompare():
    x = int(input("What's x? "))
    y = int(input("What's y? "))
    if x != y:
        print("x does not equal y")
    else: 
        print("x = y")

#using the and conditional to grade student work 
def badscorer():
    score =  int(input("Score: "))
    if score >= 90 and score <= 100:
        print("Grade: A")
    elif score >= 80 and score < 90: 
        print("Grade: B")
    elif score >= 70 and score < 80: 
        print("Grade: C")
    elif score >= 60 and score <70: 
        print("Grade: D")
    else: 
        print("You failed, git gud.")

def betterscorer():
    score = int(input("Score: "))
    if 90 <= score <= 100: 
        print("Grade: A")
    elif 80 <= score < 90: 
        print("Grade: B")
    elif 70 <= score < 80: 
        print("Grade: C")
    elif 60 <= score <= 70: 
        print("Grade: D")
    else: 
        print("You failed, git gud.")

def bestscorer():
    score = int(input("Score: "))
    if score >= 90: 
        print("Grade: A")
    elif score >= 80: 
        print("Grade B")
    elif score >= 70: 
        print("Grade C")
    elif score >= 60:
        print("Grade: D")
    else: 
        print("You failed, git gud.")

#getting into arithmetic and conditionals + - * / %
def badevenoddchecker():
    x = int(input("What's x? "))
    if x % 2 == 0:
        print("Even")
    else: 
        print("Odd")

#creating my own even odd checker with bools
def is_even(x):
    if x % 2 ==0:
        return True
    else: 
        return False

def betterevenoddchecker():
    x = int(input("What is x? "))
    if is_even(x) == True:
        print("Is even.")
    else:
        print("Is odd.")

#doing the same thing in a more pythonic manner
def better_is_even(x):
    return True if x % 2 == 0 else False

#because of the inherently boolean nature of the problem no need for if else
def best_is_even(x):
    return (x % 2 == 0)

#working with the match conditional 
def bad_match():
    name = input("What's your name? ")

    if name == "Jack":
        print("Urania")
    if name == "Daniel":
        print("Urania")
    elif name == "Nathalie":
        print("Sunflower")
    else:
        print("Who?")

#using the match operator to do the equivalent of using a dictionary, weird. 
def better_match():
    name = input("What's your name? ")

    match name: 
        case "Jack":
            print("Urania")
        case "Daniel":
            print("Urania")
        case "Nathalie":
            print("Sunflower")
        case _: 
            print("Who? ")

def best_match():
    name = input("What's your name? ")

    match name: 
        case "Jack" | "Daniel":
            print("Urania")
        case "Nathalie":
            print("Sunflower")
        case _:
            print("Who?")
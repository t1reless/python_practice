def exception_bootcamp():
    while True:
#checking for errors 
        try:
            x = int(input("What's x? "))
#handling if the user doesn't pass an int
        except ValueError:
            print("Please type a float value, not a string")
#if try runs then run all in the code block below x
#if try does throw an error then don't run else
        else:
            print(f"x is {x}")
            break

def tighter_exception_bootcamp():
    while True:
        try:
            return int(input("What's x?"))
        except ValueError:
#I can also write pass in if need be
#This keeps prompting the user but provides no feedback 
            print("Integer please...")

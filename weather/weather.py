import requests
import json 
import time 
def main():

#retrieve the public facing IP from wherever i am
    def ip_fetch():
        my_ip = requests.get("https://api.ipify.org").text
        return my_ip

#creating a function to pull lat and long from ipv4
    def location(ipv4add):
        url = "https://api.ipgeolocation.io/ipgeo?apiKey="
        key = ""
        ip = f"&ip={ipv4add}"
        return (requests.get(url+key+ip)).json()
    
#converting retrieved temp to fahrenheit as god intended
    def unit_converter(ktemp):
        ftemp = round((((ktemp-273.15)*9)/5)+32)
        return ftemp

#query open weather database to give current temp   
#tried NOAA but they didn't provide exact temp just forecast and API
#quickly rate limited. not viable for my usescase
    def weather_machine():
            location_metadata = location(ip_fetch())
            lat, long, city = location_metadata["latitude"],location_metadata["longitude"],location_metadata["city"]
            open_weather_key = ""
            raw_data =  requests.get(f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={long}&appid={open_weather_key}")
            temp_kelvin = float(raw_data.json()["main"]["temp"])
            ftemp = (unit_converter(temp_kelvin))
            
            city_temp = (f"{city} {ftemp}")
            
            return city_temp

#writing the queried temperature to a file where waybar will then read it 
#waybar is set to update every 1 min whereas this is set to every 5 to stop from getting rate 
#limited
    def file_writer(s=300):
        while True:
            with open("your_text_file","w") as file:
                file.write(str(weather_machine()))
            time.sleep(s)
            
    file_writer()

main()

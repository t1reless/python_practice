///statement of purpose & basic instructions
in order to use this project you will need to add a custom module to the waybar json file which executes a cat on the text file you update with the script. you can of course use your own form of IO but this is what was most efficient for me.
!!this project does not contain its own UI!!

you will also need api keys for: 
    ipgeolocation.io & openweathermap.org

///sample json module for waybar config
"custom/weather": {
    "exec": "cat your_file_path"
    "format":"{}°F"
    "return-type":"",
    "interval": 1
},